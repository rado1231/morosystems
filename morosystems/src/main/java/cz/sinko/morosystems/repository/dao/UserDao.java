package cz.sinko.morosystems.repository.dao;

import cz.sinko.morosystems.repository.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
public interface UserDao extends JpaRepository<User, Long>{
    
    User findByUsername(String username);
    
}
