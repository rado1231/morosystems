package cz.sinko.morosystems.service;

import cz.sinko.morosystems.service.dto.UserCreateUpdateDto;
import cz.sinko.morosystems.service.dto.UserDto;
import cz.sinko.morosystems.service.dto.UserListDto;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
public interface UserService {
    
    UserDto createUser(UserCreateUpdateDto userDto);
    
    UserDto getUserById(Long id);
    
    UserDto updateUser(Long id, UserCreateUpdateDto userDto);
    
    UserListDto getAllUsers();

    void deleteUser(Long id);
    
}
