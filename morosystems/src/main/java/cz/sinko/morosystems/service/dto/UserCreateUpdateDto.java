package cz.sinko.morosystems.service.dto;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
public class UserCreateUpdateDto implements Serializable {

    @NotBlank(message = "Name should not be blank")
    @Size(min = 4, max = 30, message = "Name '${validatedValue}' must be between {min} and {max} characters long")
    private String name;

    @NotBlank(message = "Username should not be blank")
    @Size(min = 4, max = 30, message = "Username '${validatedValue}' must be between {min} and {max} characters long")
    private String username;

    @NotBlank(message = "Password should not be blank")
    @Size(min = 4, max = 30, message = "Password '${validatedValue}' must be between {min} and {max} characters long")
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
