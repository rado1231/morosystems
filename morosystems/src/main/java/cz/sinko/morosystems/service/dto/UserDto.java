package cz.sinko.morosystems.service.dto;

import java.io.Serializable;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
public class UserDto extends UserCreateUpdateDto implements Serializable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
