package cz.sinko.morosystems.service.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
public class UserListDto implements Serializable {

    private List<UserDto> users;

    public UserListDto() {
    }

    public UserListDto(List<UserDto> users) {
        this.users = users;
    }

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

}
