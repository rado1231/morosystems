package cz.sinko.morosystems.web.rest;

import cz.sinko.morosystems.service.UserService;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
@RestController
@Validated
@RequestMapping(value = "/secured/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class SecuredUserEndpoint {

    @Autowired
    private UserService userService;
    
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@NotNull @PathVariable("id") Long id) {
        userService.deleteUser(id);
    }

}