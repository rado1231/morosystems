package cz.sinko.morosystems.web.rest;

import cz.sinko.morosystems.service.UserService;
import cz.sinko.morosystems.service.dto.UserCreateUpdateDto;
import cz.sinko.morosystems.service.dto.UserDto;
import cz.sinko.morosystems.service.dto.UserListDto;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radovan Sinko <sinko@takeplace.eu>
 */
@RestController
@Validated
@RequestMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserEndpoint {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUserById(@NotNull @PathVariable("id") Long id) {
        return userService.getUserById(id);
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto createUser(@NotNull @Valid @RequestBody UserCreateUpdateDto userDto) {
        return userService.createUser(userDto);
    }
    
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public UserListDto getAllUsers() {
        return userService.getAllUsers();
    }
    
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UserDto updateUser(@NotNull @PathVariable("id") Long id, @NotNull @Valid @RequestBody UserCreateUpdateDto userDto) {
        return userService.updateUser(id, userDto);
    }

}
