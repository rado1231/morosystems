-- Initial SQL to check if flyway is working correctly;

-- DROP SEQUENCE public.hibernate_sequence;

CREATE SEQUENCE public.hibernate_sequence
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;