-- -- Fix sequence bug due to incorrect import of testing data;
-- -- Correct import should look like:
-- -- INSERT INTO public.users
-- -- (id, name)
-- -- VALUES(nextval('hibernate_sequence'), 'Jožko Mrkvička');

SELECT setval('hibernate_sequence', (SELECT MAX(id) FROM public.users));