-- -- Add username and password;

ALTER TABLE public.users ADD username varchar(255) NULL ;
ALTER TABLE public.users ADD password varchar(255) NULL ;

UPDATE public.users
SET "name"='Jožko Mrkvička', username='jozko', password='heslo1'
WHERE id=0;

UPDATE public.users
SET "name"='Juraj Podmanický', username='juraj', password='heslo2'
WHERE id=1;

UPDATE public.users
SET "name"='Tomáš Hafner', username='tomas', password='heslo3'
WHERE id=2;

ALTER TABLE public.users ALTER COLUMN username SET NOT NULL ;
ALTER TABLE public.users ALTER COLUMN password SET NOT NULL ;